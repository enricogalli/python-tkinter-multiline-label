import tkinter as tk
from tkinter import font
#################################
#                               #
#       tkinter                 #
#       MULTILINE LABEL         #
#       by Enrico Galli         #
#       http://www.egw.it       #
#                               #
#################################



class AutoLabel(tk.Label):
    def __init__(self, target, **kwargs):
        super().__init__(target, **kwargs)

        self.target = target
        self.text_src = self['text']
        self['text'] = ''
        self.width = 0

        self.font = ""
        self.font_ruler = ""
        self.text_src_length = ""

        self.tollerance = 20

        self.pending = False

        self.bind('<Configure>', lambda e: self.after_idle(self.textAdjust))



    def textAdjust(self):

        current_width = self.winfo_width()

        if self.width == 0 or (self.width == current_width):

            font_default = ["TkDefaultFont", 12, 'normal']

            font_data = self['font'].split()

            i = 0
            for value in font_data:
                font_default[i] = value
                i += 1

            family, size, _ = font_default

            self.font_ruler = font.Font(family=family, size=size)
            self.text_src_length = self.font_ruler.measure(self.text_src)

            self.width = self.winfo_width()

            final_text = ''
            riga = ''

            if self.text_src_length > self.width:

                for word in self.text_src.split():
                    next_addition = " " + word

                    if self.font_ruler.measure(riga + next_addition) >= (self.width - self.tollerance):
                        final_text += riga + '\n'
                        riga = ''
                    else:
                        riga += next_addition

                if len(riga) > 0:
                    final_text += riga

            self['text'] = final_text

        else:
            if self.pending:
                self.after_cancel(self.pending)

            self.width = current_width
            self.pending = self.after(200, self.textAdjust)


    def pack(self, **kwargs):
        if 'fill' not in kwargs.keys():
            kwargs['fill'] = tk.X
        super().pack(kwargs)


    def place(self, **kwargs):
        if 'relwidth' not in kwargs.keys():
            kwargs['relwidth'] = 1
        super().place(kwargs)
